package ru.t1.amsmirnov.taskmanager.api;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface ILoggerService {

    void log(@NotNull final String entity) throws IOException;

}
