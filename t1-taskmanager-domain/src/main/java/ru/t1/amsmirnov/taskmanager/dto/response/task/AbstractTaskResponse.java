package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;

public abstract class AbstractTaskResponse extends AbstractResultResponse {

    @Nullable
    private TaskDTO task;

    protected AbstractTaskResponse() {

    }

    protected AbstractTaskResponse(@Nullable final TaskDTO task) {
        this.task = task;
    }

    protected AbstractTaskResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    @Nullable
    public TaskDTO getTask() {
        return task;
    }

    public void setTask(@Nullable final TaskDTO task) {
        this.task = task;
    }

}