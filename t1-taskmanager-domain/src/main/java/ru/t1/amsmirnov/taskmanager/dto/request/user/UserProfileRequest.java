package ru.t1.amsmirnov.taskmanager.dto.request.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class UserProfileRequest extends AbstractUserRequest {

    public UserProfileRequest() {
    }

    public UserProfileRequest(@Nullable final String token) {
        super(token);
    }

}