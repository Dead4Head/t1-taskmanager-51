package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class UserLogoutResponse extends AbstractResultResponse {

    public UserLogoutResponse() {
    }

    public UserLogoutResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
