package ru.t1.amsmirnov.taskmanager.dto.response;

import org.jetbrains.annotations.NotNull;

public class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
