package ru.t1.amsmirnov.taskmanager.enumerated.log;

public enum OperationType {

    DELETE,
    INSERT,
    UPDATE;

}
