package ru.t1.amsmirnov.taskmanager.dto.request.server;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class ServerUpdateSchemeRequest extends AbstractUserRequest {

    public ServerUpdateSchemeRequest() {
    }

    public ServerUpdateSchemeRequest(@Nullable String token) {
        super(token);
    }
    
}
