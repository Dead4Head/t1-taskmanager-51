package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.ITaskDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.repository.dto.ProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.repository.dto.TaskDtoRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class TaskRepositoryTest extends AbstractUserOwnedRepositoryTest {

    @NotNull
    private static IProjectDtoRepository PROJECT_REPOSITORY;

    @NotNull
    private static final ProjectDTO projectUserAlfa = new ProjectDTO();

    @NotNull
    private List<TaskDTO> tasks;

    @NotNull
    private List<TaskDTO> alfaTasks;

    @NotNull
    private List<TaskDTO> betaTasks;

    @NotNull
    private ITaskDtoRepository taskRepository;

    @BeforeClass
    public static void initData() {
        projectUserAlfa.setName("Test task name");
        projectUserAlfa.setDescription("Test task description");
        projectUserAlfa.setUserId(USER_ALFA_ID);
        PROJECT_REPOSITORY = new ProjectDtoRepository(ENTITY_MANAGER);
        TRANSACTION.begin();
        PROJECT_REPOSITORY.add(projectUserAlfa);
        TRANSACTION.commit();
    }

    @Before
    public void initRepository() throws Exception {
        tasks = new ArrayList<>();
        ENTITY_MANAGER = CONNECTION_SERVICE.getEntityManager();
        TRANSACTION = ENTITY_MANAGER.getTransaction();
        taskRepository = new TaskDtoRepository(ENTITY_MANAGER);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("TaskDTO : " + (NUMBER_OF_ENTRIES - i));
            task.setDescription("Description: " + i);
            if (i <= 5)
                task.setUserId(USER_ALFA_ID);
            else
                task.setUserId(USER_BETA_ID);
            if (i % 3 == 0)
                task.setProjectId(projectUserAlfa.getId());
            try {
                TRANSACTION.begin();
                tasks.add(task);
                taskRepository.add(task);
                TRANSACTION.commit();
            } catch (final Exception e) {
                TRANSACTION.rollback();
                throw e;
            }
        }
        alfaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() throws Exception {
        try {
            TRANSACTION.begin();
            taskRepository.removeAll();
            tasks.clear();
            TRANSACTION.commit();
        } catch (final Exception e) {
            TRANSACTION.rollback();
            throw e;
        }
    }

    @AfterClass
    public static void clearProjects() {
        try {
            TRANSACTION.begin();
            PROJECT_REPOSITORY.removeAll();
            TRANSACTION.commit();
        } catch (final Exception e) {
            TRANSACTION.rollback();
        }
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Test task name");
        task.setDescription("Test task description");
        task.setUserId(USER_ALFA_ID);
        try {
            TRANSACTION.begin();
            taskRepository.add(task);
            TRANSACTION.commit();
            tasks.add(task);
            List<TaskDTO> actualTasks = taskRepository.findAll();
            assertEquals(tasks, actualTasks);
        } catch (final Exception e) {
            TRANSACTION.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAll() throws Exception {
        try {
            assertNotEquals(0, taskRepository.findAll().size());
            TRANSACTION.begin();
            taskRepository.removeAll();
            TRANSACTION.commit();
            assertEquals(0, taskRepository.findAll().size());
        } catch (final Exception e) {
            TRANSACTION.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAllForUser() throws Exception {
        try {
            assertNotEquals(0, taskRepository.findAll(USER_ALFA_ID).size());
            TRANSACTION.begin();
            taskRepository.removeAll(USER_ALFA_ID);
            TRANSACTION.commit();
            assertEquals(0, taskRepository.findAll(USER_ALFA_ID).size());
        } catch (final Exception e) {
            TRANSACTION.rollback();
            throw e;
        }
    }

    @Test
    public void testFindAll() {
            assertNotEquals(taskRepository.findAll(USER_ALFA_ID), taskRepository.findAll(USER_BETA_ID));

            assertEquals(alfaTasks, taskRepository.findAll(USER_ALFA_ID));
            assertEquals(betaTasks, taskRepository.findAll(USER_BETA_ID));
    }

    @Test
    public void testFindAllComparator() {
            alfaTasks.sort(TaskSort.BY_NAME.getComparator());
            assertEquals(alfaTasks, taskRepository.findAllSorted(USER_ALFA_ID, "name"));

    }

    @Test
    public void testFindOneById() {
            for (final TaskDTO task : tasks) {
                assertEquals(task, taskRepository.findOneById(task.getUserId(), task.getId()));
            }
            assertNull(taskRepository.findOneById(NONE_ID));

    }


    @Test
    public void testFindAllByProjectId() throws Exception {
        final List<TaskDTO> alfaTasksWithProject = alfaTasks
                .stream()
                .filter(t -> projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<TaskDTO> betaTasksWithProject = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID) && projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());

            final List<TaskDTO> alfaRepoTasks = taskRepository.findAllByProjectId(USER_ALFA_ID, projectUserAlfa.getId());
            final List<TaskDTO> betaRepoTasks = taskRepository.findAllByProjectId(USER_BETA_ID, projectUserAlfa.getId());
            assertNotEquals(alfaRepoTasks, betaRepoTasks);
            assertEquals(alfaTasksWithProject, alfaRepoTasks);
            assertEquals(betaTasksWithProject, betaRepoTasks);

            assertEquals(new ArrayList<>(), taskRepository.findAllByProjectId(USER_ALFA_ID, NONE_ID));

    }

    @Test
    public void testUpdate() throws Exception {
        try {

            assertEquals(tasks.get(0), taskRepository.findOneById(tasks.get(0).getId()));
            tasks.get(0).setName("UPDATED_NAME");
            TRANSACTION.begin();
            taskRepository.update(tasks.get(0));
            TRANSACTION.commit();
            assertEquals(tasks.get(0).getName(), taskRepository.findOneById(tasks.get(0).getId()).getName());
        } catch (final Exception e) {
            TRANSACTION.rollback();
            throw e;
        }
    }

}
