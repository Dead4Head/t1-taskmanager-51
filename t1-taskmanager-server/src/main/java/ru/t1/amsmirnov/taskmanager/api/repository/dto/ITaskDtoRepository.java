package ru.t1.amsmirnov.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDtoRepository extends IAbstractUserOwnedDtoRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAll(@NotNull String userId, @NotNull String projectId);

}
