package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataBinSaveRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataBinSaveResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataBinSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    public static final String DESCRIPTION = "Save data to binary dump file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE BINARY]");
        @NotNull final DataBinSaveRequest request = new DataBinSaveRequest(getToken());
        @NotNull final DataBinSaveResponse response = getDomainEndpoint().saveDataBin(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
